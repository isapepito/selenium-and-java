package coreJava;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class Student {
	
	 //Attributes of students
	
	
	int studentID;
	String firstName;
	String middleName;
	String lastName;
	DateFormat df = new SimpleDateFormat ("MM/dd/yyyy");
	Date dt = new Date();
	String DOB = df.format(dt);
	String fullName = firstName + middleName + lastName;
	
	//Constructor
	public Student (int studentID, String firstName, String middleName, String lastName, String DOB)
	{
		this.studentID = studentID;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.DOB = DOB;
	}
	
	public void fullName()
	{
		System.out.println(firstName + " " + middleName + " "+ lastName );
	}
	
	//Getter and setter to have public access
		public double getstudentID(){return studentID;}
		public void setstudentID(int studentID) {this.studentID = studentID;}
		public String getfirstName(){return firstName;}
		public void setfirstName(String fisrtName) {this.firstName = fisrtName;}
		
		public String getmiddleName(){return middleName;}
		public void setmiddleName(String middleName) {this.middleName = middleName;}
		
		public String getlastName(){return lastName;}
		public void setlastname(String lastName) {this.lastName = lastName;}
		
		public String getDOB(){return DOB;}
		public void setDOB(String DOB) {this.DOB = DOB;}
		
		public String getFullName()
		{
			return fullName;
		}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Random num = new Random();
		int lim = 500;
		
		Student st1 = new Student (num.nextInt(lim), "Isabel", "Niebla", "Lopez", "03/07/1995");
		Student st2 = new Student (num.nextInt(lim), "Benito", "Arturo", "Niebla", "20/09/1961");
		Student st3 = new Student (num.nextInt(lim), "Iliana", "Rocio", "Lopez", "27/10/1961");
		
		st1.fullName();
		System.out.println(st1.getDOB());
		System.out.println("Student ID is :" + st1.getstudentID());
		st2.fullName();
		System.out.println(st2.getDOB());
		System.out.println("Student ID is :" + st2.getstudentID());
		st3.fullName();
		System.out.println(st3.getDOB());
		System.out.println("Student ID is :" + st3.getstudentID());
		
		

	}
	

}

