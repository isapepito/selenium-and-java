package coreJava;

import java.util.ArrayList;

public class Exceptionhandling {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
			int a=5;
			int b=0;
			
			//One try can be followed by multiple catch blocks
			//catch block should always be placed after try block
			try {
				
			int k = a/b;
			
			System.out.println(k);

			}
			catch (Exception e)
			{
				System.out.println("I caught the exception for Dividing a number by 0");
			}
			
			//String exception
			String st1 = "Hot";
		    String st2 = null;
		        if(st1 == null || st2 == null )
		        {
		            throw new NullPointerException("One string is missing");
		        }

			
			
	}

}
