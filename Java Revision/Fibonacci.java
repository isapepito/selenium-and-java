package coreJava;

public class Fibonacci {

	public static void main(String[] args) {


		System.out.println("Fibonacci Series for the first 8 numbers");
		fibonacciSeries (8);
	 	System.out.println("Fibonacci Series for the first 25 numbers");
	 	fibonacciSeries (25);

	}
	static void fibonacciSeries (int N)
	{
		int num1 = 0;
		int num2 = 1;
		int i = 0;
		while (i<N)
		{
			System.out.println(num1);
			int num3 = num2 + num1;
			num1 = num2;
			num2 = num3;
			i ++;
		}
	}
}
