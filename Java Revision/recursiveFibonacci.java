package coreJava;

import java.util.Scanner;

public class recursiveFibonacci {
	static Scanner in = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Select the number till you want to check the Fibbonnaci Series ");
		int fibNum = in.nextInt();
		System.out.println("The Fibbonacci Series for the first selected numbers is: 10");
		for(int i = 0; i<fibNum; i++)
		{
			System.out.println(fibonacciRecursion(i) + " ");
		}

	}
	
	static int fibonacciRecursion (int N)
	{
		if (N==0)
		{
			return 0;
		}
		
		if (N==1 || N==2)
		{
			return 1;
		}
		return fibonacciRecursion(N-2) + fibonacciRecursion(N-1);
	}
	

}
