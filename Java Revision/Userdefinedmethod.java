package coreJava;

import java.util.Scanner;

public class Userdefinedmethod {
	static Scanner scan = new Scanner(System.in);
	
	public static int[] getMaxNumber()
	{
		System.out.println("Enter three numbers from 1 to 30");
		int number [] = new int [3];
		number [0] = scan.nextInt();
		number [1] = scan.nextInt();
		number [2] = scan.nextInt();
		
		
		int k = 0;
		int max = number[k];
		
		while (k<number.length)
		{
			if (number[k]>max)
			{
				max = number[k];
			}
			k++;
		}
		System.out.println("The Max number is: " + max);
		return number;
		
	}
	
	public static int getGreatestNumber(int num1, int num2, int num3)
	{
		
		int [] num = new int [3];
		num[0] = num1;
		num[1] = num2;
		num[2] = num3;
		
		int i = 0;
		int greatest = num[i];
		
		while (i<num.length)
		{
			if (num[i] > greatest) 
			{
				greatest = num[i];
			}
			i++;
		}
		return greatest;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		//This will print 3 numbers without user
		System.out.println(getGreatestNumber(23,12,19));
		System.out.println(getGreatestNumber(122,98,377));
		
		//This will print customized 3 numbers
		System.out.println(getMaxNumber());

		

	}

}
