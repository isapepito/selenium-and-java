# Automation Testing Workshop
This project contains the different Topics with their respective assessments, from the Automation Testing Workshop.


## TOPICS
1. GitLab SetUp and usage
2. SCRUM Overview and Jira Setup
3. Java Revision
4. JUnit
5. Assertion
6. Log4j
7. HTNL
8. XPath
9. Selenium
10. Automation Framework
11. Software Testing Overview
12. Exercise Demo
13. Interview Pratice
