package Exercise4.Selenium;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

@DisplayName("Test Filling Form Page")
public class FormpageTest {
	
	WebDriver driver;
	Formpage fp;
	SubmittedForm sf;
	
	@Before
	
	public void setup()
	{
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\ALIENWARE\\Documents\\drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		//Get URL
		driver.get("https://formy-project.herokuapp.com/form");
	}
	
	@Test
	public void testLogin()
	{
		//Fill form page
		fp = new Formpage(driver);
		fp.firstName().sendKeys("Isabel");
		fp.lastName().sendKeys("Niebla");
		fp.jobTittle().sendKeys("IT Trainee");
		fp.setEducationalLevel("g");
		fp.setSex("f");
		fp.setYearsOfExp(0);
		fp.opt2.click();
		fp.date().click();
		fp.date().sendKeys("07/03/95");
		fp.submitButton().click();
		//Submitted Form page
		sf = new SubmittedForm(driver);
		assertEquals(sf.getSuccessfulSubmit(), "The form was successfully submitted!");
	}
	
	@After
	public void quit()
	{
		driver.quit();
	}
}
