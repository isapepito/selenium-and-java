package Exercise4.Selenium;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


@DisplayName("Test Excel Reading and Wiritng test")
	
	
public class TestExercise5 {
	WebDriver driver;
	
	@Before
	public void setup()
	{
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\ALIENWARE\\Documents\\drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		//Get URL
		driver.get("https://formy-project.herokuapp.com/form");
		
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testFillForm() throws InterruptedException, IOException {
		
		ExcelRead excelData = new ExcelRead(); 
		ArrayList<String> data = excelData.getData(); 
		String firstName = data.get(0);
		String lastName = data.get(1);
		String jobTitle = data.get(2);
		String educationLvl = data.get(3);
		String sex = data.get(4);
		String yearsOfExp = data.get(5);
		
		FormpageEx5 fp = new FormpageEx5(driver);
		fp.submitForm(driver,firstName,lastName,jobTitle,educationLvl,sex,yearsOfExp);
		assertEquals(fp.getSuccessfulSubmit(), "The form was successfully submitted!");
		
	}

	@After // QUIT BROWSER
	public void out(){
		driver.quit();
	}

}
