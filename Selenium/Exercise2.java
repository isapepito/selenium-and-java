import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Exercise2 {

	public static void main(String[] args) throws InterruptedException {
		//Create Web Driver
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\ALIENWARE\\Documents\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		//Select URL
		driver.get("https://formy-project.herokuapp.com/form");
		//Filling the form
		WebElement firstName = driver.findElement(By.id("first-name"));
		firstName.sendKeys("Isabel");
		WebElement lastName = driver.findElement(By.id("last-name"));
		lastName.sendKeys("Niebla");
		WebElement jobTitle = driver.findElement(By.id("job-title"));
		jobTitle.sendKeys("IT Trainee");
		WebElement educationLevel = driver.findElement(By.id("radio-button-3"));
		educationLevel.click();
		WebElement sex = driver.findElement(By.id("checkbox-2"));
		sex.click();
		WebElement yearsOfExp = driver.findElement(By.id("select-menu"));
		yearsOfExp.click();
		driver.findElement(By.xpath("//select/option[2]")).click();
		WebElement date = driver.findElement(By.id("datepicker"));
		date.click();
		date.sendKeys("07/03/95");
		WebElement submitButton = driver.findElement(By.cssSelector("a[class='btn btn-lg btn-primary']"));
		submitButton.click();
		Thread.sleep(1500);
		driver.quit();
	}

}
