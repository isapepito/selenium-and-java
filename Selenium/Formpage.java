package Exercise4.Selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Formpage {
	
	WebDriver driver;
	public Formpage(WebDriver driver) 
	{	
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id=("first-name"))
	WebElement firstName;
	
	@FindBy(id=("last-name"))
	WebElement lastName;
	
	@FindBy(id=("job-title"))
	WebElement jobTitle;
	
	String educationalLevel;

	String sex;

	int yearsOfExp;
	
	@FindBy(xpath=("//select/option[2]"))
	WebElement opt2;
	
	@FindBy(id=("datepicker"))
	WebElement date;
	
	@FindBy(css=("a[class='btn btn-lg btn-primary']"))
	WebElement submitButton;
	

	public WebElement firstName()
	{
		return firstName;
	}
	
	public WebElement lastName()
	{
		return lastName;
	}
	
	public WebElement jobTittle()
	{
		return jobTitle;
	}
	
	public void setEducationalLevel(String educationalLevel){
		this.educationalLevel = educationalLevel;
		
		if(educationalLevel.equalsIgnoreCase("h"))
		{
			driver.findElement(By.id("radio-button-1")).click();
		}
		else if (educationalLevel.equalsIgnoreCase("c"))
		{
			driver.findElement(By.id("radio-button-2")).click();
		}
		else
		{
			driver.findElement(By.id("radio-button-3")).click();
		}
	}
	
	public void setSex(String sex)
	{
		this.sex = sex;
		if (sex.equalsIgnoreCase("m"))
		{
			driver.findElement(By.id("checkbox-1")).click();
		}
			else if (sex.equalsIgnoreCase("f")) 
			{
				driver.findElement(By.id("checkbox-2")).click();
			}
			else 
			{
				driver.findElement(By.id("checkbox-3")).click();
			}
	}
	
	public void setYearsOfExp(int yearsOfExp)
	{
		this.yearsOfExp = yearsOfExp;
		if (yearsOfExp>10)
		{
			driver.findElement(By.xpath("//select/option[5]")).click();
		}
		else if(yearsOfExp>=5 && yearsOfExp<=9)
		{
			driver.findElement(By.xpath("//select/option[4]")).click();
		}
		else if(yearsOfExp>=2 && yearsOfExp<=4)
		{
			driver.findElement(By.xpath("//select/option[3]")).click();
		}
		else if(yearsOfExp>=0 && yearsOfExp<=1)
		{
			driver.findElement(By.xpath("//select/option[2]")).click();
		}
	}
	
	public WebElement date()
	{
		return date;
	}
	
	public WebElement submitButton()
	{
		return submitButton;
	}
}
