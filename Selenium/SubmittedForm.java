package Exercise4.Selenium;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SubmittedForm {
	
	
	WebDriver driver;
	public SubmittedForm(WebDriver driver) 
	{	
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(css=("div[class='alert alert-success']"))
	WebElement successfulSubmit;
	
	public String getSuccessfulSubmit()
	{
		//Explicit wait
		WebDriverWait expWait = new WebDriverWait(driver, Duration.ofSeconds(5));
		expWait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[class='alert alert-success']")));
		String message = driver.findElement(By.cssSelector("div[class='alert alert-success']")).getText();
		return message;
	}
	
}
