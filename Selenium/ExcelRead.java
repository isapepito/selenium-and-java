package Exercise4.Selenium;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;

public class ExcelRead {
	String filePath = "C:\\Users\\ALIENWARE\\Documents\\Trabajo\\TATA\\WorkshopAutomation\\Exercise5.xls"; 
	String firstName; 
	String lastName; 
	String jobTittle;
	String educationLvl;
	String sex; 
	String yearsOfExp;
	
	
	public ArrayList<String> getData() throws IOException {
		File file = new File(filePath); 
		FileInputStream fis = new FileInputStream(file);
		HSSFWorkbook wb = new HSSFWorkbook(fis); 
		HSSFSheet sheet = wb.getSheet("1"); 
		
		Iterator<Row> row = sheet.rowIterator(); 
		row.next(); 
		ArrayList<String> data = new ArrayList<String>(); 
		HSSFRow eachRow = sheet.getRow(1); 

		firstName = eachRow.getCell(0).getStringCellValue(); 
		data.add(firstName);
		lastName = eachRow.getCell(1).getStringCellValue();
		data.add(lastName);
		jobTittle = eachRow.getCell(2).getStringCellValue();
		data.add(jobTittle);
		educationLvl = eachRow.getCell(3).getStringCellValue();
		data.add(educationLvl);
		sex = eachRow.getCell(4).getStringCellValue();
		data.add(sex);
		yearsOfExp = eachRow.getCell(5).getStringCellValue();
		data.add(yearsOfExp);
		
		return data; 
	}

}
