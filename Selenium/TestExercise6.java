package Excel.ApachePOI;
import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class TestExercise6 {
	@Test
	public void fillTheForm() throws IOException{
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\ALIENWARE\\Documents\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		FormPageXLS fp = new FormPageXLS();
		fp.Form(driver);
		
		WebDriverWait expWait = new WebDriverWait(driver, Duration.ofSeconds(5));
		expWait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[class='alert alert-success']")));
		String message = driver.findElement(By.cssSelector("div[class='alert alert-success']")).getText();

		assertEquals(driver.findElement(By.cssSelector("div[class='alert alert-success']")).getText(), "Thanks for submitting your form");
		System.out.println("The test was successful");
		
		File screenshot4 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(screenshot4, new File(".//screenshot//SubmitButton.png"));
		
	}


}
