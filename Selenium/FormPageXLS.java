package Excel.ApachePOI;
import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class FormPageXLS {
	
	public static void Form(WebDriver driver) throws IOException
	{
		
		XlsReader reader = new XlsReader(".\\datafiles\\Exercise5.xlsx");
		String sheetName = "Sheet1";
		
		String firstName = reader.getCellData(sheetName, 1, 1);
		String laststName = reader.getCellData(sheetName, 1, 2);
		String jobTitle = reader.getCellData(sheetName, 1, 3);
		String levelEducation = reader.getCellData(sheetName, 1, 4);
		String sex = reader.getCellData(sheetName, 1, 5);
		String yearsExp = reader.getCellData(sheetName, 1, 6);
		String date = reader.getCellData(sheetName, 1, 7);
		
		driver.get("http://formy-project.herokuapp.com/form");
		driver.findElement(By.id("first-name")).sendKeys(firstName);
		driver.findElement(By.id("last-name")).sendKeys(laststName);
		driver.findElement(By.id("job-title")).sendKeys(jobTitle);
		
		if (levelEducation == "High School") {
			driver.findElement(By.id("radio-button-1")).click();
		} 
		else if(levelEducation == "College") {
			driver.findElement(By.id("radio-button-2")).click();
		} 
		else if(levelEducation == "Graduated"){
			driver.findElement(By.id("radio-button-3")).click();
		}
		
		if (sex == "Male") {
			driver.findElement(By.id("checkbox-1")).click();
		} 
		else if(sex == "Female") {
			driver.findElement(By.id("checkbox-2")).click();
		} 
		else {
			driver.findElement(By.id("checkbox-3")).click();
		}

		//FIRST SCREENSHOT AFTER FILLING SEX CHECKBOX
		File screenshot1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(screenshot1, new File(".//screenshot//SexFilled.png"));
		
		driver.findElement(By.id("select-menu")).click();
		
		
		if (yearsExp == "0" || yearsExp == "1") {
			driver.findElement(By.xpath("//option[2]")).click();
		} 
		else if(yearsExp == "2" || yearsExp == "3" || yearsExp == "4") {
			driver.findElement(By.xpath("//option[3]")).click();
		} 
		else if(yearsExp == "5" || yearsExp == "6" || yearsExp == "7"|| yearsExp == "8" || yearsExp == "9") {
			driver.findElement(By.xpath("//option[4]")).click();
		}
		else {
			driver.findElement(By.xpath("//option[5]")).click();
		}
		
		//SECOND SCREENSHOT AFTER FILLING YEARS OF EXP
		File screenshot2 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(screenshot2, new File(".//screenshot//YearsofExpFilled.png"));
		
		driver.findElement(By.id("datepicker")).click();
		driver.findElement(By.className("today")).click();
		
		//THIRD SCREENSHOT AFTER FILLING DATE
		File screenshot3 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(screenshot3, new File(".//screenshot//Datepicker.png"));
		driver.findElement(By.cssSelector("a[href='/thanks']")).click();	
	}

}

